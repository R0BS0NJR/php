# README

## Manual MarkDown

### Nivel

Este es un manual sobre sintaxis MarkDown, al contrario de los editores como LibreOffice que son WYSIWYG.

### Negrita

Esto es una **negrita**.

### Cursiva

Esto es _cursiva_.

### Listas

**Listas**

* Elemento
* Elemento
* Elemento

Lista ordenada:


1. Uno
2. Dos
3. Tres

### Código

```php
<?php

    echo "Hola Mundo!;

?>
```

Código en una sola línea es asi `echo "Hola Mundo."`

### Enlace

Un **enlace** se hace así [Texto del enlace](https://www.w3schools.com/php/phptryit.asp?filename=tryphp_intro)

### Imagen

Incrustar una imagen:


![texto alt](IMG/Luna.jpg)

---

> Esto es una cita de Marx.